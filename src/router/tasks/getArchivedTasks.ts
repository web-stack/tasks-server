import { mapTaskDTO, TaskDTO } from "@app/dto/TaskDTO";
import { getCollection } from "@db/index";
import { Request, Response } from "express";

async function getArchivedTasks(req: Request, res: Response) {
  try {
    const collection = await getCollection();
    
    const data = await collection.find({ archived: true }).toArray();
    const result: TaskDTO[] = data.map(mapTaskDTO);
    
    return res.json({
      code: "ok",
      payload: result,
    });
  } catch (e) {
    return res.json({
      code: "error",
      message: "could not retrieve archived tasks",
      error: e,
    });
  }
}

export default getArchivedTasks