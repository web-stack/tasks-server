import { getCollection } from "@db/index";
import { Request, Response } from "express";
import { ObjectId } from "mongodb";

async function deleteTask(req: Request, res: Response) {
  try {
    const taskId = req.body.taskId
    if (!taskId) {
      throw new Error("taskId is not present in the request body")
    }
    
    const collection = await getCollection()
    await collection.deleteOne({ _id: new ObjectId(taskId) })
    
    return res.json({
      code: "ok",
      payload: taskId
    })
  } catch (e) {
    return res.json({
      code: "error",
      message: "could not delete task",
      error: e,
    })
  }
}

export default deleteTask