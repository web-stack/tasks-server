import { TaskDTO } from "@app/dto/TaskDTO";
import { getCollection } from "@db/index";
import { Request, Response } from "express";

async function createTask(req: Request, res: Response) {
  try {
    const { title, description, expireDate } = req.body;
    const taskDTO: TaskDTO = {
      title,
      description,
      expireDate,
      done: false,
      archived: false
    };
    
    const collection = await getCollection();
    const result = await collection.insertOne(taskDTO)
    
    return res.json({
      code: "ok",
      payload: result.ops[0],
    });
  } catch (e) {
    return res.json({
      code: "error",
      message: "could not add task",
      error: e,
    });
  }
}

export default createTask