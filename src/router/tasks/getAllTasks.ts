import { mapTaskDTO, TaskDTO } from "@app/dto/TaskDTO";
import { getCollection } from "@db/index";
import { Request, Response } from "express";

async function getAllTasks(req: Request, res: Response) {
  try {
    const collection = await getCollection();
    
    const data = await collection.find({
      $or: [
        { archived: null },
        { archived: false },
      ]
    }).toArray();
    const result: TaskDTO[] = data.map(mapTaskDTO);
    
    return res.json({
      code: "ok",
      payload: result,
    });
  } catch (e) {
    return res.json({
      code: "error",
      message: "could not retrieve all tasks",
      error: e,
    });
  }
}

export default getAllTasks