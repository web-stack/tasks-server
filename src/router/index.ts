import express from "express";
import oauth from "@app/middleware/oauth";
import taskHandler from "./Task.handler";
import createTask from "./tasks/createTask";
import deleteTask from "./tasks/deleteTask";
import getAllTasks from "./tasks/getAllTasks";
import getArchivedTasks from "./tasks/getArchivedTasks";

const router = express.Router();

router.use(oauth);
router.get("/task", (req, res) => {
  return res.json({ code: "ok", message: "test" });
});

router.get("/tasks/all", getAllTasks);
router.put("/tasks/add", createTask);
router.post("/tasks/finish", taskHandler.markTaskAsDone);
router.post("/tasks/archive", taskHandler.archiveTask);
router.post("/tasks/delete", deleteTask);
router.get("/tasks/getArchived", getArchivedTasks);

export default router;
