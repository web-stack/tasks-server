import { Request, Response } from "express";
import { getCollection } from "@db/index";
import { ObjectId } from "mongodb";

async function markTaskAsDone(req: Request, res: Response) {
  try {
    const taskId = req.body.taskId;
    if (!taskId) {
      throw new Error("taskId is not present in the request body");
    }

    const collection = await getCollection();
    const filter = { _id: new ObjectId(taskId), };
    const updateQuery = {
      $set: {
        done: true,
      },
    };
    const options = { returnOriginal: false, };
    const { value } = await collection.findOneAndUpdate(
      filter,
      updateQuery,
      options
    );

    return res.json({
      code: "ok",
      payload: value,
    });
  } catch (e) {
    return res.json({
      code: "error",
      message: "could not mark task as done",
      error: e,
    });
  }
}

async function archiveTask(req: Request, res: Response) {
  try {
    const taskId = req.body.taskId
    if (!taskId) {
      throw new Error("taskId is not present in the request body")
    }
    
    const collection = await getCollection()
    const filter = { _id: new ObjectId(taskId) }
    const updateQuery = {
      $set: { archived: true, },
    };
    const options = { returnOriginal: false, }
    
    const { value } = await collection.findOneAndUpdate(filter, updateQuery, options)
    return res.json({
      code: "ok",
      payload: value,
    });
    
  } catch (e) {
    return res.json({
      code: "error",
      message: "could not archive task",
      error: e,
    })
  }
}

export default {
  markTaskAsDone,
  archiveTask
};
