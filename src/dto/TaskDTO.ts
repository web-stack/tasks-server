export interface TaskDTO {
  _id?: string;
  title: string;
  description: string;
  expireDate: string;
  done: boolean;
  archived: boolean;
}

export function mapTaskDTO(body: any): TaskDTO {
  return {
    _id: body._id,
    title: body.title,
    description: body.description,
    expireDate: body.expireDate,
    done: body.done,
    archived: body.archived
  };
}
