import express from "express";
import bodyParser from "body-parser";
import router from "@app/router";
import cors from 'cors';

const app = express();

app.use(cors())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());
app.use(router);

const port = process.env.PORT || 3001;
app.listen(port, () => {
  console.log("[APP]", `started at ${port}`);
});
