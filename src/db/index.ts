import mongodb, { Collection, MongoClient } from "mongodb";
import dbConfig from "@config/db";

export async function connect(): Promise<MongoClient> {
  return await mongodb.MongoClient.connect(dbConfig.url, {
    useUnifiedTopology: true,
  });
}

export async function getCollection(): Promise<Collection> {
  const client = await connect();
  const database = client.db(dbConfig.name);
  return database.collection(dbConfig.collectionName);
}
