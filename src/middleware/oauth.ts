import { NextFunction, Request, Response } from "express";
import oauthConfig from "@config/oauth";
import axios from "axios";

async function middleware(req: Request, res: Response, next: NextFunction) {
  const authHeader = req.header("Authorization");
  if (!authHeader) {
    return res.json({ code: "error", message: "Authentication failed" });
  }

  const token = authHeader.split("Bearer ")[1];
  const { data } = await axios.post(
    `${oauthConfig.url}/oauth/authorize`,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  if (data.code !== "ok") {
    return res.json({ code: "error", message: "Authentication failed" });
  }

  next();
}

export default middleware;
