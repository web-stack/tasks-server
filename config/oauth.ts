const OAUTH_SERVER_URL = process.env.OAUTH_SERVER_URL || "127.0.0.1"

export default {
  url: `http://${OAUTH_SERVER_URL}:3000`,
};
