const MONGODB_URL = process.env.MONGO_DB_URL || "127.0.0.1";

export default {
  url: `mongodb://${MONGODB_URL}:27017/`,
  name: "task_manager_db",
  collectionName: "tasks",
};
